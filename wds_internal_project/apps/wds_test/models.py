from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    class Role(models.TextChoices):
        ADMIN = 'ADMIN', 'Admin'
        USER = 'USER', 'User'

    phone = models.CharField(max_length=12, blank=True)
    address = models.CharField(max_length=256, blank=True)
    payment = models.BooleanField(default=False)
    team = models.ManyToManyField('Team', blank=True)
    role = models.CharField(max_length=50, choices=Role.choices, default=Role.USER)


class UserManager(BaseUserManager):
    """
    add filter of only user
    """
    def get_queryset(self, *args, **kwargs):
        result = super().get_queryset(*args, **kwargs)
        return result.filter(role=User.Role.USER)


class User(CustomUser):
    """
    a proxy class - this means that this class this table won't generate,
    but we could work to extend methods for this class
    """
    base_role = CustomUser.Role.USER

    user = UserManager()

    class Meta:
        proxy = True


class Subscription(models.Model):
    class Type(models.TextChoices):
        MONTH = 'Month', 'Month'
        YEAR = 'Year', 'Year'

    type = models.CharField(max_length=50, choices=Type.choices)

    def __str__(self):
        return self.type


class Invoice(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.PROTECT)
    subscription = models.ForeignKey(Subscription, on_delete=models.PROTECT)

    def save(self, *args, **kwargs):
        user = CustomUser.objects.get(pk=self.user.pk)
        user.payment = True
        user.save()
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.user.username


class Team(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
