from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'invoice', views.InvoiceViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('users/', views.UserListView.as_view(), name='users'),
    path('create_user/', views.CreateUserView.as_view(), name='create_user'),
    path('update_user/<int:pk>/', views.UpdateUserView.as_view(), name='update_user'),
]
