from django.contrib import admin
from .models import Subscription, Invoice, CustomUser, Team


class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['username', 'role', 'payment']

    def get_exclude(self, request, obj=None):
        excluded = super().get_exclude(request, obj) or []
        if not obj:  # if it's a create, so no object yet
            return excluded + ['phone', 'address', 'payment', 'role']
        return excluded

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(role=request.user.role)


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Subscription)
admin.site.register(Invoice)
admin.site.register(Team)
