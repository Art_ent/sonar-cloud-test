from rest_framework import serializers

from .models import User, Invoice, Team


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['pk', 'email', 'username', 'password', 'role', 'phone', 'address', 'payment', 'team']


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'username', 'password']
        extra_kwargs = {'password': {'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
        )

        return user


class UpdateUserSerializer(serializers.ModelSerializer):
    team = serializers.PrimaryKeyRelatedField(queryset=Team.objects.all(), many=True, )
    queryset = User.objects.all()

    class Meta:
        model = User
        fields = ('username', 'email', 'role', 'phone', 'address', 'payment', 'team')

    def update(self, instance, validated_data):
        instance.username = validated_data['username']
        instance.email = validated_data['email']
        instance.role = validated_data['role']
        instance.phone = validated_data['phone']
        instance.address = validated_data['address']
        instance.payment = validated_data['payment']
        instance.team.set(validated_data['team'])

        instance.save()

        return instance


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'
