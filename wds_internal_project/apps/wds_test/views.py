from rest_framework import viewsets, generics, permissions, authentication

from .serializers import UserSerializer, InvoiceSerializer, UpdateUserSerializer, CreateUserSerializer
from .models import Invoice, User


class UserListView(generics.ListAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.user.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    authentication_classes = [authentication.SessionAuthentication, authentication.BasicAuthentication]


class CreateUserView(generics.CreateAPIView):
    queryset = User.user.all()
    serializer_class = CreateUserSerializer
    permission_classes = [permissions.BasePermission]


class UpdateUserView(generics.RetrieveUpdateAPIView):
    queryset = User.user.all()
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = UpdateUserSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = [permissions.BasePermission]
