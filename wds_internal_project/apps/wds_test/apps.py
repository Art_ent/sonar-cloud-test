from django.apps import AppConfig


class WdsTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wds_test'
