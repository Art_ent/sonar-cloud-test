# Pull base image
# the official Python image that already has all the tools and packages that we need for our Django application
FROM python:3.11-slim-bullseye
# set work directory
WORKDIR /code
# Set environment variables
# means Python will not try to write .pyc files
ENV PYTHONDONTWRITEBYTECODE 1
# ensures our console output is not buffered by Docker
ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
# Install dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt
# Copy project
COPY . .
